﻿using IdentityModel.Client;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text.Json;
using System.Threading.Tasks;

namespace Realbridge.Quickstart
{
    class Program
    {
        #region Configurations

        /// <summary>
        /// Your unique ClientID. 
        /// </summary>
        static readonly string _clientId = "YOUR_CLIENT_ID";

        /// <summary>
        /// Your unique ClientSecret.
        /// </summary>
        static readonly string _clientSecret = "YOUR_CLIENT_SECRET";

        /// <summary>
        /// Realbridge prod env: https://login.realbridge.se
        /// Realbridge dev env: https://login2.realbridge.se
        /// Local dev: https://localhost:5000
        /// </summary>
        static readonly string _identityEndpoint = "https://login2.realbridge.se";

        /// <summary>
        /// Realbridge prod env: https://services.realbridge.se
        /// Realbridge dev env: https://services2.realbridge.se
        /// Local dev: https://localhost:5002
        /// </summary>
        static readonly string _apiEndpoint = "https://services2.realbridge.se";
        static readonly string _apiVersion = "v1";

        /// <summary>
        /// Realbridge Services API scope.
        /// </summary>
        static readonly string _scope = "rbs";

        /// <summary>
        /// Your Realbridge tenant.
        /// </summary>
        static readonly string _tenant = "REQUESTED_TENANT";

        /// <summary>
        /// The site within the tenant.
        /// </summary>
        static readonly string _site = "REQUESTED_SITE";

        /// <summary>
        /// Key to the DataSchema which will be used for the item import.
        /// </summary>
        static readonly string _dataSchemaKey = "DATA_SCHEMA_KEY";

        static readonly JsonSerializerOptions _jsonSerializerOptions = new JsonSerializerOptions
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            IgnoreNullValues = true
        };

        #endregion

        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                await BatchImportItems();
            }).GetAwaiter().GetResult();
        }

        static async Task BatchImportItems()
        {
            try
            {
                // Setup and authenticate the Realbridge client.
                var client = await GetClient();

                // Prepare data for the API request.
                var request = new ApiRequest<DataSchemaBatchImportModel>(GenerateData());
                var content = new StringContent(JsonSerializer.Serialize(request, _jsonSerializerOptions));
                content.Headers.ContentType = new MediaTypeHeaderValue(MediaTypeNames.Application.Json);

                // Make the API request.
                var requestUri = $"{_apiEndpoint}/api/{_apiVersion}/{_tenant}/{_site}/elements/schemas/{_dataSchemaKey}/batch";
                var apiResponse = await client.PostAsync(requestUri, content);
                if (apiResponse.IsSuccessStatusCode)
                {
                    // Read and handle the response.
                    var obj = JsonSerializer.Deserialize<object>(await apiResponse.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

        /// <summary>
        /// Initialize machine-to-machine authentication with Realbridge and return the prepared client.
        /// </summary>
        /// <returns></returns>
        static async Task<HttpClient> GetClient()
        {
            var client = new HttpClient();
            var discovery = await client.GetDiscoveryDocumentAsync(_identityEndpoint);
            var response = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = discovery.TokenEndpoint,
                ClientId = _clientId,
                ClientSecret = _clientSecret,
                Scope = _scope
            });

            client.SetBearerToken(response.AccessToken);

            return client;
        }

        /// <summary>
        /// Replace this with your own data.
        /// </summary>
        /// <returns></returns>
        static DataSchemaBatchImportModel GenerateData()
        {
            return new DataSchemaBatchImportModel
            {
                Items = new[]
                {
                    new DataSchemaImportItemModel
                    {
                        Action = DataSchemaImportAction.Upsert,
                        Properties = new Dictionary<string,object>()
                        {
                            {
                                "some_string_val",
                                "abc"
                            },
                            {
                                "some_number_val",
                                123
                            },
                            {
                                "some_bool_val",
                                true
                            }
                        }
                    }
                }
            };
        }
    }
}