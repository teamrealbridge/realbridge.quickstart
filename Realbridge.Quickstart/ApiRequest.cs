﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Realbridge.Quickstart
{
    public class ApiRequest<T>
    {
        public T Data { get; set; }

        public ApiRequest(T data)
        {
            Data = data;
        }
    }
}
