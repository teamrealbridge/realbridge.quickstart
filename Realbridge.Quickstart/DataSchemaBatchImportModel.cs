﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Realbridge.Quickstart
{
    public enum DataSchemaImportAction
    {
        /// <summary>
        /// Insert or update existing items.
        /// </summary>
        Upsert,

        /// <summary>
        /// Only insert non-existing items.
        /// </summary>
        Create,

        /// <summary>
        /// Updates existing items.
        /// </summary>
        Update,

        /// <summary>
        /// Delete items.
        /// </summary>
        Delete
    }

    public class DataSchemaBatchImportModel
    {
        public IEnumerable<DataSchemaImportItemModel> Items { get; set; }
    }

    public class DataSchemaImportItemModel
    {
        public DataSchemaImportAction Action { get; set; }

        public object Properties { get; set; }
    }
}
